class Car{
    name;
    speed;
    brand;
    color;

    constructor(name, speed, brand, color) {
        this.color = color
        this.brand = brand
        this.name = name
        this.speed = speed
    }
   
    drive(){
        console.log(this.name + ' is driving at ' + this.speed + ' km/h');
    }
    paint = (newColor) => {
        console.log('Current color: ' + this.color + '. Changing to ' + newColor);
        this.color = newColor;
    }
}

const fadil = new Car('Fadil', 100, 'VinFast', 'red');
const luxSa = new Car('LuxSa', 120, 'VinFast', 'black');

fadil.drive();
luxSa.drive();

fadil.paint("blue")
console.log(fadil.color)

var arr1 = ['Hi!'];
var arr2 = ['Hi!'];
var arr1str = JSON.stringify(arr1);
var arr2str = JSON.stringify(arr2);
console.log(arr1str === arr2str);