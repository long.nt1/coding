// const apps = document.createElement("div")
// apps.id = "app"
const app = document.getElementById("app")
const clocks = []

const title = document.createElement("div")
title.classList.add("title")
title.innerHTML = "Đồng Hồ Bấm Giờ"

const btnAdd = document.createElement("button")
btnAdd.classList.add("control")
btnAdd.innerHTML = "Add"
btnAdd.addEventListener("click", () => {
    const clock = new Clock()
    clocks.push(clock)
    app.appendChild(clock.render())
})

const btnAllStart = document.createElement("button")
btnAllStart.classList.add("control")
btnAllStart.innerHTML = "Start All"
btnAllStart.addEventListener("click", () => {
    for (let i=0; i < clocks.length; i++) {
        clocks[i].start()
    }
})

app.appendChild(title);
app.appendChild(btnAdd)
app.appendChild(btnAllStart)