class Clock {
    $container
    $timer
    $btnStart
    $btnStop
    $btnPause

    minute = 0
    second = 0
    milisecond
    timeInterval = null

    constructor() {
        this.$container = document.createElement("div")
        this.$container.classList.add("container")

        this.$timer = document.createElement("div")
        this.$timer.classList.add("timer")
        this.$timer.innerHTML = "00:00:000"

        this.$btnStart = document.createElement("button")
        this.$btnStart.classList.add("btnStart")
        this.$btnStart.innerHTML = "Start"
        this.$btnStart.addEventListener("click", this.start)
        
        this.$btnPause = document.createElement("button")
        this.$btnPause.classList.add("btnPause")
        this.$btnPause.innerHTML = "Pause"
        this.$btnPause.addEventListener("click", this.pause)

        this.$btnStop = document.createElement("button")
        this.$btnStop.classList.add("btnStop")
        this.$btnStop.innerHTML = "Stop"
        this.$btnStop.addEventListener("click", this.stop)

        this.$container.appendChild(this.$timer)
        this.$container.appendChild(this.$btnStart)
        this.$container.appendChild(this.$btnPause)
        this.$container.appendChild(this.$btnStop)

        this.minute= 0
        this.second = 0
        this.milisecond = 0
    }

    start = () => {
        this.timeInterval = setInterval(() => {
            this.milisecond++ 
            this.updateTime()
        }, 10)
    }

    pause = () => {
        if (this.timeInterval) {
            clearInterval(this.timeInterval)
        }
    }

    stop = () => {
        if (this.timeInterval) {
            clearInterval(this.timeInterval)
        }
        this.milisecond = 0
        this.second = 0
        this.minute = 0
        this.updateTime()
    }

    updateTime() {
        if (this.milisecond > 99) {
            this.second++
            this.milisecond = 0
        }
        if (this.second > 59) {
            this.minute++
            this.second = 0
        }
        this.$timer.innerHTML = this.minute + ":" + this.second + ":" + this.milisecond
    }

    render() {
        this.$container.appendChild(this.$timer)
        this.$container.appendChild(this.$btnStart)
        this.$container.appendChild(this.$btnPause)
        this.$container.appendChild(this.$btnStop)
        return this.$container
    }
}