import { setScreen } from "./Components/set_screen.js";
import { Chat } from "./Components/chat.js";
import { Login } from "./Components/login.js";

firebase.auth().onAuthStateChanged((user) =>{
    if(user){
        const chat = new Chat();
        setScreen(chat);
    } else{
        const login = new Login();
        setScreen(login);    
    }
});