let currentScreen = null;
const app = document.getElementById('app');

const setScreen = (screen)=>{
    if (currentScreen !== null){
        app.removeChild(currentScreen);
    }
    currentScreen = app.appendChild(screen.render());
    console.log(currentScreen)
};

export { setScreen };