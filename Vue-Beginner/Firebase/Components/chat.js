import { SideBar } from "./sideBar.js"
import { TitleBar } from "./titleBar.js"
import { MessageArea } from "./messageArea.js"
import { InfoPanel } from "./in4Panel.js"

class Chat {
    $container
    $nav
    $sideBar
    $titleBar
    $messageArea
    $infoPanel
    $body 

    $body__title
    $title__div1
    $title__div2
    $title__label
    $title__button

    $body__section
    $section__chat
    $chat__message
    $chat__input
    $section__in4

    activeConversation

    constructor() {
        this.$container = document.createElement("div")
        this.$container.classList.add("container")

        this.$sideBar = new SideBar(this.setActiveConversation)
        this.$titleBar =  new TitleBar()
        this.$messageArea = new MessageArea()
        this.$infoPanel = new InfoPanel()


        this.$nav = document.createElement("div")
        this.$nav.classList.add("nav")

        this.$body = document.createElement("div")
        this.$body.classList.add("body")

        this.$body__title = document.createElement("div")
        this.$body__title.classList.add("body__title")

        this.$title__div1 = document.createElement("div")
        this.$title__div2 = document.createElement("div")

        this.$title__label = document.createElement("label")
        this.$title__label.classList.add("title__label")

        this.$title__button = document.createElement("button")
        this.$title__button.classList.add("title__button")
        this.$title__button.innerHTML = "Log Out"
        this.$title__button.addEventListener("click", this.handleLogout)

        this.$body__section = document.createElement("div")
        this.$body__section.classList.add("body__section")

        this.$section__chat = document.createElement("div")
        this.$section__chat.classList.add("section__chat")

        this.$chat__message = document.createElement("div")
        this.$chat__message.classList.add("chat__message")

        this.$chat__input = document.createElement("div")
        this.$chat__input.classList.add("chat__input")

        this.$section__in4 = document.createElement("div")
        this.$section__in4.classList.add("section__in4")

        this.activeConversation = null
    }

    handleLogout = () => {
        firebase.auth().signOut()
    }

    setActiveConversation = (conversation) => {
        console.log(conversation);
        this.activeConversation = conversation;
        this.$titleBar.setName(this.activeConversation.name);
        this.$sideBar.setConversation(this.activeConversation);
        this.$messageArea.setConversation(this.activeConversation)
    }

    render() {
        // body__section_chat
        this.$chat__message.appendChild(this.$messageArea.render())
        this.$section__chat.appendChild(this.$chat__message)
        this.$section__chat.appendChild(this.$chat__input)

        // body__section
        this.$section__in4.appendChild(this.$infoPanel.render())
        this.$body__section.appendChild(this.$section__chat)
        this.$body__section.appendChild(this.$section__in4)

        // body__title__div
        this.$title__label.appendChild(this.$titleBar.render())
        this.$title__div1.appendChild(this.$title__label)
        this.$title__div2.appendChild(this.$title__button)

        // body__title
        this.$body__title.appendChild(this.$title__div1)
        this.$body__title.appendChild(this.$title__div2)

        // body
        this.$body.appendChild(this.$body__title)
        this.$body.appendChild(this.$body__section)

        // // nav
        this.$nav.appendChild(this.$sideBar.render())

        // container
        this.$container.appendChild(this.$nav)
        this.$container.appendChild(this.$body)
        return this.$container;
    }
}

export { Chat }