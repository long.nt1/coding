import { MessageItem } from "./messageItem.js";

class MessageArea {
    $container
    $messageList
    $composer
    $input
    $btn

    activeConversation 
    MessageSubscribe

    constructor() {
        this.$container = document.createElement("div")

        this.$messageList = document.createElement("div")

        this.$composer = document.createElement("div")

        this.$input = document.createElement("input")
        this.$input.type = "text"
        this.$input.placeholder = "Please be nice in chat <33"
        this.$input.addEventListener("keypress", this.sendMessage)

        this.$btn = document.createElement("button")
        this.$btn.type = "button"
        this.$btn.innerHTML = "Send"
        this.$btn.addEventListener("click", this.handleSendMessage)
    }

    setConversation(conversation) {
        this.activeConversation = conversation
        this.$messageList.innerHTML = ""

        if (this.MessageSubscribe) {
            this.MessageSubscribe()
        }

        this.MessageSubscribe = db
        .collection("Messages")
        .where("conversationId", "==", this.activeConversation.id)
        .onSnapshot(this.messageListener)
    }

    messageListener = (snapshot) => {
        snapshot.docChanges().forEach((change) => {
            const data = change.doc.data()
            const $messageItem = new MessageItem(data.sender, data.content)
            this.$messageList.appendChild($messageItem.render())
        })
    }

    handleSendMessage = (event) => {
        event.preventDefault()
        if (this.activeConversation) {
            db.collection("Messages")
            .add({
                sender: firebase.auth().currentUser.email,
                content: this.$input.value,
                conversationId: this.activeConversation.id,
            })
        } else {
            alert("you must select conversation first")
        }
    }

    sendMessage = (event) => {
        if (event.key === "Enter") {
            this.$btn.click()
        }
    }

    render() {
        this.$composer.appendChild(this.$input);
        this.$composer.appendChild(this.$btn);

        this.$container.appendChild(this.$messageList);
        this.$container.appendChild(this.$composer);

        return this.$container;
    }
}

export { MessageArea }