class CreateConversationModal {
    $container
    $modalContent
    $form
    $input
    $btnCreate
    $btnCancel

    constructor() {
        this.$container = document.createElement("div")
        this.$container.style.display = "none"
        this.$container.classList.add('modal-container');
        this.$container.addEventListener("click", this.handleClose)

        this.$modalContent = document.createElement("div")
        this.$modalContent.classList.add("modal-content")

        this.$form = document.createElement("div")
        this.$form.style.display = "flex"

        this.$input = document.createElement("input")
        this.$input.type = "text"
        this.$input.placeholder = "Enter conversation name ..."

        this.$btnCreate = document.createElement("button")
        this.$btnCreate.type = "button"
        this.$btnCreate.innerHTML = "Create"
        this.$btnCreate.addEventListener("click", this.handleSubmit)

        this.$btnCancel = document.createElement("button")
        this.$btnCancel.type = "button"
        this.$btnCancel.innerHTML = "Cancel"
        this.$btnCancel.addEventListener("click", this.handleCancel)
    }

    handleSubmit = (event) => {
        event.preventDefault()
        db.collection("Conversations")
        .add({
            name: this.$input.value,
            createBy: firebase.auth().currentUser.email,
            users: [firebase.auth().currentUser.email],
        })
        .then(() => {
            this.setVisible(false)
            this.$input.value = ""
        })
    }

    handleCancel = () => {
        this.setVisible(false)
        this.$input.value = ""
    }

    handleClose = (event) => {
        if (event.target == this.$container) {
            this.setVisible(false)
            this.$input.value = ""
        }
    }

    setVisible(visible) {
        if (visible) {
            this.$container.style.display = "flex"
        } else {
            this.$container.style.display = "none"
        }
    }

    render() {
        this.$form.appendChild(this.$input)
        this.$form.appendChild(this.$btnCreate)
        this.$form.appendChild(this.$btnCancel)

        this.$modalContent.appendChild(this.$form)

        this.$container.appendChild(this.$modalContent)
        return this.$container
    }
}

export { CreateConversationModal }