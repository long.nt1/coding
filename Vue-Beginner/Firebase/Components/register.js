import { InputGroup } from "./input_group.js";
import { Login } from "./login.js"
import { setScreen } from "./set_screen.js"

class Register {
    $container 
    $title 

    $form_register
    $input_nickName
    $input_email
    $input_password
    $input_confirmPassword

    $feedBack_msg
    $btnSubmit
    $linkToLogin

    constructor() {
        this.$container = document.createElement("div")
        this.$container.classList.add("center", "h-screen", "flex-col")

        this.$title = document.createElement("h3")
        this.$title.innerHTML = "Register"

        this.$form_register = document.createElement("form")
        
        this.$input_nickName = new InputGroup("text", "Nick Name", "displayName")
        this.$input_email = new InputGroup("email", "Email", "email")
        this.$input_password = new InputGroup("password", "Password", "password")
        this.$input_confirmPassword = new InputGroup("password", "Confirm Password", "confirmPassword")

        this.$feedBack_msg = document.createElement("div")

        this.$btnSubmit = document.createElement("button")
        this.$btnSubmit.type = "submit"
        this.$btnSubmit.addEventListener("click", this.handleSubmit)
        this.$btnSubmit.innerHTML = 'Register'

        this.$linkToLogin = document.createElement("div")
        this.$linkToLogin.classList.add("btn-link")
        this.$linkToLogin.addEventListener("click", this.moveToLogin)
        this.$linkToLogin.innerHTML = "Back to login"
    }

    handleSubmit = (event) => {
        event.preventDefault();

        const email = this.$input_email.getInputValue();
        const displayName = this.$input_nickName.getInputValue();
        const password = this.$input_password.getInputValue();
        const confirmPassword = this.$input_confirmPassword.getInputValue();

        this.$input_email.setError(null);
        this.$input_password.setError(null);
        this.$input_nickName.setError(null);
        this.$input_confirmPassword.setError(null);

        if (!email) {
            this.$input_email.setError('Email cannot be empty!');
        }
        if (!displayName) {
            this.$input_nickName.setError('Display name cannot be empty');
        }
        if (password.length < 6) {
            this.$input_password.setError('Password length must be greater or equal than 6!');
        }
        if (confirmPassword != password) {
            this.$input_confirmPassword.setError('Confirm password not matched!');
        }

        else if (password === confirmPassword) {
            firebase.auth().createUserWithEmailAndPassword(email, password)
            .then(() => {
                this.$feedbackMessage.innerHTML = 'Register successfully! Please check your inbox';
                firebase.auth().currentUser.sendEmailVerification();
                this.$input_email.setInputValue('');
                this.$input_nickName.setInputValue('');
                this.$input_password.setInputValue('');
                this.$input_confirmPassword.setInputValue('');
            })
            .catch((error) => {
                this.$feedbackMessage.innerHTML = error.toString();
                console.log(error);
            })
        }
    }

    moveToLogin = () => {
        const login = new Login()
        setScreen(login)
    }

    render() {
        this.$form_register.appendChild(this.$input_email.render());
        this.$form_register.appendChild(this.$input_nickName.render());
        this.$form_register.appendChild(this.$input_password.render());
        this.$form_register.appendChild(this.$input_confirmPassword.render());
        this.$form_register.appendChild(this.$btnSubmit);

        this.$container.appendChild(this.$title)
        this.$container.appendChild(this.$form_register)
        this.$container.appendChild(this.$feedBack_msg)
        this.$container.appendChild(this.$linkToLogin)
        return this.$container
    }
} 

export {Register}