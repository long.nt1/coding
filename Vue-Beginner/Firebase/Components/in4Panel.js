class InfoPanel {
    $container
    $form
    $input
    $btn
    $userList

    constructor() {
        this.$container = document.createElement("div")
        
        this.$form = document.createElement("form")

        this.$input = document.createElement("input")
        this.$input.type = "email"
        this.$input.placeholder = "Enter email to add"

        this.$btn = document.createElement("button")
        this.$btn.type = "button"
        this.$btn.innerHTML = "Add"

        this.$userList = document.createElement("ul")
    }

    render() {
        this.$form.appendChild(this.$input)
        this.$form.appendChild(this.$btn)

        this.$container.appendChild(this.$form)
        this.$container.appendChild(this.$userList)
        return this.$container
    }
}

export { InfoPanel }