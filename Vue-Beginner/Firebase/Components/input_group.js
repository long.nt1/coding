class InputGroup {
    $container
    $label
    $input
    $error_msg

    constructor(type, label, name) {
        this.$container = document.createElement("div")
        this.$container.classList.add("input-group")

        this.$input = document.createElement("input")
        this.$input.type = type
        this.$input.name = name

        this.$label = document.createElement("label")
        this.$label.innerHTML = label

        this.$error_msg = document.createElement("div")
        this.$error_msg.classList.add("error-msg")
    }

    getInputValue() {
        return this.$input.value
    }

    setInputValue(newValue) {
        this.$input.value = newValue
    }

    setError(message) {
        if (message) {
            this.$error_msg.innerHTML = message
            this.$container.classList.add('has-error');
        } 
        else {
            this.$error_msg.innerHTML = '';
            this.$container.classList.remove('has-error');
        }
    }

    render() {
        this.$label.appendChild(this.$input)
        this.$container.appendChild(this.$label)
        this.$container.appendChild(this.$error_msg)
        return this.$container
    }
}

export {InputGroup}
