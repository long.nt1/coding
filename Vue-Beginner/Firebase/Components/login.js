import { InputGroup } from "./input_group.js"
import { Register } from "./register.js"
import { setScreen } from "./set_screen.js"

class Login {
    $container
    $title

    $form_login
    $input_email
    $input_password

    $btnSubmit
    $linkToRegister

    constructor() {
        this.$container = document.createElement("div")
        this.$container.classList.add('center', 'h-screen', 'flex-col');

        this.$title = document.createElement("h3")
        this.$title.innerHTML= "Login"

        this.$input_email = new InputGroup("email", "Email", "email")
        this.$input_password = new InputGroup("password", "Password", "password")

        this.$form_login = document.createElement("form")

        this.$btnSubmit = document.createElement("button")
        this.$btnSubmit.type = "submit"
        this.$btnSubmit.innerHTML = "Login"
        this.$btnSubmit.addEventListener("click", this.handleSubmit)

        this.$linkToRegister = document.createElement("div")
        this.$linkToRegister.classList.add("btn-link")
        this.$linkToRegister.addEventListener("click", this.moveToRegister)
        this.$linkToRegister.innerHTML = "Create a new acccount ?"
    }

    handleSubmit = (evt) => {
        evt.preventDefault()
        const email = this.$input_email.getInputValue()
        const password = this.$input_password.getInputValue()

        firebase.auth().signInWithEmailAndPassword(email, password)
        .then ((userIn4) => {
            console.log(userIn4)
        })
        .catch ((error) => {
            console.log(error)
        })
    }

    moveToRegister = () => {
        const register = new Register()
        setScreen(register)
    }

    render() {
        this.$form_login.appendChild(this.$input_email.render())
        this.$form_login.appendChild(this.$input_password.render())
        this.$form_login.appendChild(this.$btnSubmit)

        this.$container.appendChild(this.$title)
        this.$container.appendChild(this.$form_login)
        this.$container.appendChild(this.$linkToRegister)
        return this.$container
    }
}

export { Login }