class TitleBar {
    $container
    $textName

    constructor() {
        this.$container = document.createElement("div")
        
        this.$textName = document.createElement("div")
    }

    setName(name) {
        this.$textName.innerHTML = name
    }

    render() {
        this.$container.appendChild(this.$textName)
        return this.$container
    }
}

export { TitleBar }