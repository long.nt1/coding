var month = parseInt(prompt("Hãy nhập tháng"));
var year = parseInt(prompt("Hãy nhập năm"));

if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) {
    console.log("tháng", month, ":31 ngày")
}

if (month == 4 || month == 6 || month == 9 || month == 11) {
    console.log("tháng", month, ":30 ngày")
}

if (month == 2) {
    if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0) {
        console.log("tháng 2: 29 ngày")
    }

    else {
        console.log("tháng 2: 28 ngày")
    }
}