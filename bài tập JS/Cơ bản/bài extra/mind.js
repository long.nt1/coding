function data() {
    var day = parseInt(document.getElementById("day").value)
    var month = parseInt(document.getElementById("month").value)
    var year = parseInt(document.getElementById("year").value)

    if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) {
        if (month == 12) {
            if (day == 31) {
                console.log("ngày 1, tháng 1, năm ", (year + 1))
            }
            else if (day < 31) {
                console.log("ngày ",(day + 1), ",tháng ",(month), ",năm ",(year))
            }
        }

        else if (day == 31) {
            console.log("ngày 1", ",tháng ",(month + 1), ",năm ",(year))
        }
        
        else {
            console.log("ngày ",(day + 1), ",tháng ",(month), ",năm ",(year))
        }
    }
    
    if (month == 4 || month == 6 || month == 9 || month == 11) {
        if (day == 30) {
            console.log("ngày 1", ",tháng ",(month + 1), ",năm ",(year))
        }
        
        else {
            console.log("ngày ",(day + 1), ",tháng ",(month), ",năm ",(year))
        }
    }
    
    if (month == 2) {
        if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0) {
            if (day == 29) {
                console.log("ngày 1", ",tháng ",(month + 1), ",năm ",(year))
            }

            else if (day < 29) {
                console.log("ngày ",(day + 1), ",tháng ",(month), ",năm ",(year))
            }
        }

        else {
            if (day == 28) {
                console.log("ngày 1", ",tháng ",(month + 1), ",năm ",(year))
            }

            else if (day < 28) {
                console.log("ngày ",(day + 1), ",tháng ",(month), ",năm ",(year))
            }
        }
    }
}