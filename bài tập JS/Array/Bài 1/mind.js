var Pokemon = [
    "Bulbasaur",
    "Ivysaur",
    "Venusaur",
    "Charmander",
    "Charmeleon",
    "Charizard",
    "Squirtle",
    "Wartortle",
    "Blastoise",
    "Caterpie",
    "Metapod",
    "Butterfree",
    "Weedle",
    "Kakuna",
    "Beedrill",
    "Pidgey",
    "Pidgeotto",
    "Pidgeot",
    "Rattata",
];

console.log(Pokemon);
console.log("pokemon which can fly and use fire is: ", Pokemon[5])

function handledata() {
    Pokemon.pop();
    Pokemon.push("pikachu");
    Pokemon.unshift("Meow 2");
    console.log(Pokemon);
}
